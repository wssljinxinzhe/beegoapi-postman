# Beego Api
## Beego FrameWorkとBee ToolをGo Pathにinstall.
    go get -u github.com/beego/bee
    go get -u github.com/astaxie/beego
##
## //こちら、NoSqlDemoです。
## Beego Api Demoを作成する。
    bee api NoSqlDemo
## 
    cd $GOPATH/src/NoSqlDemo
## Beego Apiを起動。
    bee run -gendoc=true -downdoc=true
## Beego Api Router.
	func init() {
		ns := beego.NewNamespace("/v1",
			beego.NSNamespace("/object",
				beego.NSInclude(
					&controllers.ObjectController{},
				),
			),
			beego.NSNamespace("/user",
				beego.NSInclude(
					&controllers.UserController{},
				),
			),
		)
		beego.AddNamespace(ns)
	}
	
## 上記のRouterを確認、PostManで、Urlを入力。(今回Objectを例をとします。User同じように)
## PostManは、Api TestするためのApp.
##
## PostManでGET　Methodで、下のUrlをテストしたら。
	http://localhost:8080/v1/object
##
	{
 	 "hjkhsbnmn123": {
  	  "ObjectId": "hjkhsbnmn123",
  	  "Score": 100,
  	  "PlayerName": "astaxie"
  	},
  	"mjjkxsxsaa23": {
   	 "ObjectId": "mjjkxsxsaa23",
   	 "Score": 101,
   	 "PlayerName": "someone"
 	 }
	}
## Demo　Code中に、既にinitで生成したデータを表示されるはずです。
##
## PostManこのように、Restful apiで、Apiアプリケーションをテストしてくれるソフトです。